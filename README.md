
# VentilAidCPAPControlDeviceHardware
This repository contains KiCAD design files for two electronic boards fo VentilAid mk III. 
If you wish to replicate simplest version please start with only one pressure sensor in U13 or U12. 
Please refer to main repository for other suggestions.

Mind that this design is distributed with no guaranty, as described in license. 

